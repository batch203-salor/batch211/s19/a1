// console.log("Hello World");

let username;
let password;
let role;


function login(){

    username = prompt("Enter your username:");
    password = prompt("Enter your password:");
    role = prompt("Enter your role:").toLowerCase();

    //checking if all the fields are field out.
    if(username === "" || username === null || password === "" || password === null || role === "" || role === null){
        alert("Input must not be empty.")
    } else {
        switch (role){
            case "admin":
                alert("Welcome back to the class portal, admin!");
                break;
            case "teacher":
                alert("Thank you for logging in, teacher!");
                break;
            case "student":
                alert("Welcome to the class portal, student!");
                break;
            default:
                alert("Role out of range.");
                break;
        }
    }

}
    
login();


function checkAverage(grade1,grade2,grade3,grade4){


    let average = (grade1+grade2+grade3+grade4)/4;
    average = Math.round(average);
    if(role === "admin" || role === "teacher" || role === undefined || role === null){
        alert("You are not allowed to access this feature!");
    } else {
    	// to check the result if it is rounded off.
        console.log(average)
        if(average <= 74){
            console.log("Hello, student, your average is: " + average + ". The letter equivalent is F");
        } else if(average >= 75 && average <= 79){
            console.log("Hello, student, your average is: " + average + ". The letter equivalent is D");
        } else if(average >= 80 && average <= 84){
            console.log("Hello, student, your average is: " + average + ". The letter equivalent is C");
        } else if(average >= 85 && average <= 89){
            console.log("Hello, student, your average is: " + average + ". The letter equivalent is B");
        } else if(average >= 90 && average <= 95){
            console.log("Hello, student, your average is: " + average + ". The letter equivalent is A")
        } else if(average >= 96 && average <=100){
            console.log("Hello, student, your average is: " + average + ". The letter equivalent is A+")
        }
        else{
        	console.log("Grade is out of range. Check your input!")
        }
    }

}

